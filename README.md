# goldengloberace

Découvrez le monde de la voile avec la première édition de la plus célèbre régate en solitaire, autour du monde et sans escale : Golden Globe Challenge. Vous découvrez l'histoire de cette course, les participants et le parcours avec une simulation.